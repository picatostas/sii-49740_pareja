#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

#include "DatosMemCompartida.h"
 int main()
{
	int fd_bot;
	DatosMemCompartida *datos;
	struct stat bstat;

	fd_bot = open("/tmp/bot.txt",O_RDWR);


	if (fstat(fd_bot, &bstat)<0) {
    perror("Error en fstat del fichero"); close(fd_bot);
    exit(1);
  }
if ((datos=(DatosMemCompartida*)mmap(NULL,bstat.st_size,
	PROT_WRITE | PROT_READ,MAP_SHARED,fd_bot,0))== MAP_FAILED)
	{
		printf("Error de proyeccion\n");
		exit(1);
	}

	close(fd_bot);

	while(1)
	{
		
		if (datos->esfera.radio<0)
		{		
			break;
		}

		if(datos->esfera.centro.y > datos->raqueta2.centro.y)
			datos->accion=1;
		if(datos->esfera.centro.y < datos->raqueta2.centro.y)
			datos->accion=-1;
		if(datos->esfera.centro.y == datos->raqueta2.centro.y)
			datos->accion=0;

		usleep(25000);

	}
	munmap(datos,bstat.st_size);

}



  	