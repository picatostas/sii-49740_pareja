// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoClient.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	pts_total=0;
}

CMundo::~CMundo()
{
	/*
	// parte correspondiente a server

	char buff[]="?";
	write(fd_fifo,buff,strlen(buff));
	datos->esfera.radio=-1;
	*/
	datos->esfera.radio=-1;
	
	munmap(datos,bstat.st_size);
	comm.Close();
	/*
	close(fd_s2c);
	unlink("/tmp/s2c.txt");
	close(fd_c2s);
	unlink("/tmp/c2s.txt");
	*/
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char buffer[300];
	int bytes_leidos;

	//bytes_leidos=read(fd_s2c,buffer,300);
	bytes_leidos=comm.Receive(buffer,300);
	if(bytes_leidos==0)
		exit(0);
	else
	{
	 sscanf(buffer,"%f %f %f %f %f %f %f %f %f %f %f %d %d",
	  &esfera.centro.x,&esfera.centro.y,&esfera.radio,
	  &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
	  &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,
	  &puntos1, &puntos2); 
	}
	

	/*
	// parte correspondiente a SERVER

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;



	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+4*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+4*rand()/(float)RAND_MAX;
		puntos2++;

		pts_total=puntos1+puntos2;
	 	if(pts_total%2==0) esfera.setRad(esfera.radio-=0.1f);

	 	

	 	char buffer1[] = "Jugador2 ha marcado 1 punto, lleva un total de :";
	 	char buffer2[20];
	 	sprintf(buffer2,"%d puntos",puntos2);
	 	strcat(buffer1,buffer2);
	 	write(fd_fifo,buffer1,strlen(buffer1));
	 	
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-4*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-4*rand()/(float)RAND_MAX;
		puntos1++;

		pts_total=puntos1+puntos2;
	 	if(pts_total%2==0) esfera.setRad(esfera.radio-=0.1f);
	 	

	 	char buffer1[] = "Jugador1 ha marcado 1 punto, lleva un total de :";
	 	char buffer2[20];
	 	sprintf(buffer2,"%d puntos",puntos1);
	 	strcat(buffer1,buffer2);
	 	write(fd_fifo,buffer1,strlen(buffer1));
	 
	}

	*/

	// paso de datos al BOT
	 datos->esfera=esfera;
	 datos->raqueta2=jugador2;
	 if (datos->accion == 1 ) OnKeyboardDown('o',0,0);
	 if (datos->accion == -1 ) OnKeyboardDown('l',0,0);
	 if (datos->accion == 0) datos->raqueta2.velocidad.y=0;
	// printf("%.2f \n",esfera.velocidad.x );
	//

	 //char cad1[100];
	 
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		/* // Tuberias
	case 's':write(fd_c2s,"s",2);break;
	case 'w':write(fd_c2s,"w",2);break;
	case 'l':write(fd_c2s,"l",2);break;
	case 'o':write(fd_c2s,"o",2);break;
	case 'y':write(fd_c2s,"y",2);break;
	case 'n':write(fd_c2s,"n",2);break;
	case 'r':write(fd_c2s,"r",2);break;
*/
	// sockets 
	case 's':comm.Send("s",2);break;
	case 'w':comm.Send("w",2);break;
	case 'l':comm.Send("l",2);break;
	case 'o':comm.Send("o",2);break;
	case 'y':comm.Send("y",2);break;
	case 'n':comm.Send("n",2);break;
	case 'r':comm.Send("r",2);break;
	}


}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	

	 	// parte correspondiente a server

	datos = new DatosMemCompartida;
	datos->esfera = esfera;
	datos->raqueta2 = jugador2;
	datos->accion= 0;
	/*
	fd_fifo = open("/tmp/logger.txt", O_WRONLY);
	*/


	creat("/tmp/bot.txt",0777);
	fd_bot = open("/tmp/bot.txt",O_RDWR);
	write(fd_bot, datos, sizeof(DatosMemCompartida));
	delete  datos;

	if (fstat(fd_bot, &bstat)<0) {
    perror("Error en fstat del fichero"); close(fd_bot);
    exit(1);
  	}
	if ((datos=(DatosMemCompartida*)mmap(NULL,bstat.st_size,
	PROT_WRITE | PROT_READ,MAP_SHARED,fd_bot,0))== MAP_FAILED)
	{
		printf("Error de proyeccion\n");
	//	perror("");
		exit(1);
	}

	close(fd_bot);

char msg[20]="";
printf("Introducza nombre :");
scanf("%s",msg);
char user_ip[20]="";
printf("Introducza la IP de servidor:");
scanf("%s",user_ip);

comm.Connect(user_ip,2000);
comm.Send(msg,strlen(msg));


///////////////TUBERIAS/////////////////
/*
  	
  	int error1 =mkfifo("/tmp/s2c.txt",0777); // ayuda a depurar
	if (error1) 
	{
		printf("Error al crear tuberia\n");
		exit(1);
	}
	int error2=mkfifo("/tmp/c2s.txt",0777); // ayuda a depurar
	if (error2) 
	{
		printf("Error al crear tuberia\n");
		exit(1);
	}
	fd_s2c= open("/tmp/s2c.txt",O_RDONLY);
	fd_c2s= open("/tmp/c2s.txt",O_WRONLY);

*/
}
