// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x += velocidad.x*t;
	centro.y += velocidad.y*t;
}
void Esfera::setRad(float r)
{
	if (r <= 0.25f) radio=0.25f;
	else radio=r;

}
Vector2D Esfera::getPos()
{
	return centro;

}
Vector2D Esfera::getVel()
{
	return velocidad;

}
void Esfera::setVel(float v)
{
	if (v>17)
		velocidad.x=17;
	else if (v<-17)
		velocidad.x=-17;
	else 
		velocidad.x=v;

}
void Esfera::setPos(float x, float y)
{
	centro.x=x;
	centro.y=y;
}